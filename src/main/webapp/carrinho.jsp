<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Lista de Clientes</title>
<link rel="stylesheet" href="css/lojinha.css">

</head>
<body>
<h1>Carrinho de Compras</h1>


<c:choose>
	<c:when test="${empty sessionScope.carrinho}">
		<p>Carrinho vazio</p>
	</c:when>
	
	<c:otherwise>
	<p>Produtos no Carrinho</p>
	<table border="1">
	<tr>
		<th>Código</th>
		<th>Descrição</th>
		<th>********</th>
	</tr>
	
	<c:forEach items="${sessionScope.carrinho}" var="p">
		<tr>
			<td>${p.id}</td>
			<td>${p.descricao}</td>
			<td><small><a href="carrinho.sec?cmd=remove&produto=${p.id}" class="action">Remover</a></small></td>
		</tr>
	</c:forEach>
	
	</table>

	</c:otherwise>
</c:choose>
<a href="comprar.sec">Voltar</a>
</body>
</html>