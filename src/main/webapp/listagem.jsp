<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="UTF-8" />
<title>Carrinho</title>
<link rel="stylesheet" href="css/lojinha.css">
</head>
<body>
	<h2>Produtos adicionados</h2>

	<form action="ServletRemocao" method="post">
		<table>
			<thead>
				<tr>
					<th>Código</th>
					<th>Descrição</th>
					<th>Preço</th>
					<th>Remover</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="p" varStatus="produto" items="${carrinho.produtos}">
					<tr>
						<td>${p.id}</td>
						<td>${p.descricao}</td>
						<td>${p.preco}</td>
						<td><input type="checkbox" name="remove" value="${p.id}" />
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<input type="submit" name="botao" value="Remover" />
	</form>

	<a href="adicionar.jsp">Voltar</a>
</body>
</html>