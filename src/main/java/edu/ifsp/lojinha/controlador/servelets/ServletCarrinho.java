package edu.ifsp.lojinha.controlador.servelets;

import java.io.IOException;

import edu.ifsp.lojinha.modelo.Carrinho;
import edu.ifsp.lojinha.modelo.Produto;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet({ "/ServletCarrinho" })
public class ServletCarrinho extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ServletCarrinho() {
	}

	// RESPONSABILIDADE: Adicionar um produto ao carrinho de compras
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Produto novoProduto = new Produto(request.getParameter("descricao"));
		HttpSession session = request.getSession();

		Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");
		if (carrinho == null) {
			carrinho = new Carrinho();
		}

		carrinho.inserirProduto(novoProduto);
		session.setAttribute("carrinho", carrinho);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/carrinho.jsp");
		dispatcher.forward(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}
