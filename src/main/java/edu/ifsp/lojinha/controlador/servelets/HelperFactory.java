package edu.ifsp.lojinha.controlador.servelets;

import edu.ifsp.lojinha.controlador.helpers.CommandHelper;
import jakarta.servlet.http.HttpServletRequest;

public class HelperFactory {

	public CommandHelper getHelper(HttpServletRequest request) {
		String acao = request.getParameter("acao");
		String nomeClasse = request.getServletContext().getInitParameter(acao);
		
		try {
			Class<?> clazz = Class.forName(nomeClasse);
			Object obj = clazz.getDeclaredConstructor().newInstance();
			CommandHelper helper = (CommandHelper) obj;
			return helper;
		} 
		
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
